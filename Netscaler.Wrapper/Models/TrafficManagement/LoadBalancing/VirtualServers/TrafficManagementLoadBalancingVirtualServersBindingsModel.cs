﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netscaler.Wrapper.Models.TrafficManagement.LoadBalancing.VirtualServers
{
   public class TrafficManagementLoadBalancingVirtualServersBindingsModel
    {
        public int errorcode { get; set; }
        public string message { get; set; }
        public string severity { get; set; }
        public List<TrafficManagementLoadBalancingVirtualServersBindingsLbvserver_Service_Binding> lbvserver_service_binding { get; set; } = new List<TrafficManagementLoadBalancingVirtualServersBindingsLbvserver_Service_Binding>();
    }

    public class TrafficManagementLoadBalancingVirtualServersBindingsLbvserver_Service_Binding
    {
        public string name { get; set; }
        public string servicename { get; set; }
        public string stateflag { get; set; }
        public string ipv46 { get; set; }
        public int port { get; set; }
        public string servicetype { get; set; }
        public string curstate { get; set; }
        public string weight { get; set; }
        public string dynamicweight { get; set; }
        public string cookieipport { get; set; }
        public string vserverid { get; set; }
        public string vsvrbindsvcip { get; set; }
        public int vsvrbindsvcport { get; set; }
        public string preferredlocation { get; set; }
    }

}

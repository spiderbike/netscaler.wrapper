﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netscaler.Wrapper.Models.TrafficManagement.LoadBalancing.VirtualServers
{
    public class TrafficManagementLoadBalancingVirtualServersListModel
    {
        public int errorcode { get; set; }
        public string message { get; set; }
        public string severity { get; set; }
        public List<TrafficManagementLoadBalancingVirtualServersListLbvserver> lbvserver { get; set; } = new List<TrafficManagementLoadBalancingVirtualServersListLbvserver>();
    }

    public class TrafficManagementLoadBalancingVirtualServersListLbvserver
    {
        public string name { get; set; }
        public string insertvserveripport { get; set; }
        public string ipv46 { get; set; }
        public string ippattern { get; set; }
        public string ipmask { get; set; }
        public string listenpolicy { get; set; }
        public string ipmapping { get; set; }
        public int port { get; set; }
        public string range { get; set; }
        public string servicetype { get; set; }
        public string type { get; set; }
        public string curstate { get; set; }
        public string effectivestate { get; set; }
        public int status { get; set; }
        public int lbrrreason { get; set; }
        public string cachetype { get; set; }
        public string authentication { get; set; }
        public string authn401 { get; set; }
        public string dynamicweight { get; set; }
        public string priority { get; set; }
        public string clttimeout { get; set; }
        public string somethod { get; set; }
        public string sopersistence { get; set; }
        public string sopersistencetimeout { get; set; }
        public string healththreshold { get; set; }
        public string lbmethod { get; set; }
        public string backuplbmethod { get; set; }
        public string dataoffset { get; set; }
        public string health { get; set; }
        public string datalength { get; set; }
        public string ruletype { get; set; }
        public string m { get; set; }
        public string persistencetype { get; set; }
        public int timeout { get; set; }
        public string persistmask { get; set; }
        public string v6persistmasklen { get; set; }
        public string persistencebackup { get; set; }
        public int backuppersistencetimeout { get; set; }
        public string cacheable { get; set; }
        public string rtspnat { get; set; }
        public string sessionless { get; set; }
        public string trofspersistence { get; set; }
        public string map { get; set; }
        public string connfailover { get; set; }
        public string redirectportrewrite { get; set; }
        public string downstateflush { get; set; }
        public string disableprimaryondown { get; set; }
        public string gt2gb { get; set; }
        public string consolidatedlconn { get; set; }
        public string consolidatedlconngbl { get; set; }
        public int thresholdvalue { get; set; }
        public bool invoke { get; set; }
        public int version { get; set; }
        public string totalservices { get; set; }
        public string activeservices { get; set; }
        public string statechangetimesec { get; set; }
        public string statechangetimeseconds { get; set; }
        public string statechangetimemsec { get; set; }
        public string tickssincelaststatechange { get; set; }
        public string hits { get; set; }
        public string pipolicyhits { get; set; }
        public string push { get; set; }
        public string pushlabel { get; set; }
        public string pushmulticlients { get; set; }
        public string policysubtype { get; set; }
        public string l2conn { get; set; }
        public string appflowlog { get; set; }
        public bool isgslb { get; set; }
        public string icmpvsrresponse { get; set; }
        public string rhistate { get; set; }
        public string newservicerequestunit { get; set; }
        public string vsvrbindsvcip { get; set; }
        public int vsvrbindsvcport { get; set; }
        public string skippersistency { get; set; }
        public string td { get; set; }
        public string minautoscalemembers { get; set; }
        public string maxautoscalemembers { get; set; }
        public string macmoderetainvlan { get; set; }
        public string dns64 { get; set; }
        public string bypassaaaa { get; set; }
        public string processlocal { get; set; }
        public string vsvrdynconnsothreshold { get; set; }
        public string retainconnectionsoncluster { get; set; }
        public string nodefaultbindings { get; set; }
    }

}

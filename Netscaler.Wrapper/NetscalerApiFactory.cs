﻿using System;
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Netscaler.Wrapper
{
    public class NetscalerApiFactory
    {
        private readonly string _httpClientName;
        private readonly IServiceProvider _serviceProvider;

        public NetscalerApiFactory(string httpClientName, IServiceProvider serviceProvider)
        {
            _httpClientName = httpClientName;
            _serviceProvider = serviceProvider;
        }

        public NetScalerApi Create(NetScalerOptions options)
        {
            var httpClientFactory = _serviceProvider.GetRequiredService<IHttpClientFactory>();
            var httpClient = httpClientFactory.CreateClient(_httpClientName);

            httpClient.BaseAddress = new Uri(Endpoints.NetScaler(options.Host));
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{options.Username}:{options.Password}")));

            // This is a fancy way of doing new NetScalerApi(httpClient), except that it allow NetScalerApi to have
            // other constructor parameters which are injected by DI.
            var activator = ActivatorUtilities.CreateFactory(typeof(NetScalerApi), new Type[] { typeof(HttpClient), });
            return (NetScalerApi)activator(_serviceProvider, new object[] { httpClient });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;

namespace Netscaler.Wrapper
{
    public static class NetScalerServiceCollectionExtensions
    {
        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .RetryAsync(3);
        }

        public static IServiceCollection AddNetScalerServices(this IServiceCollection services)
        {
            var clientName = services.AddHttpClient<NetScalerApi>(client => client.DefaultRequestHeaders.Add("Accept", "application/json"))
                .AddPolicyHandler(GetRetryPolicy())
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (request, certificate, chain, errors) =>
                    {
                        if (errors == SslPolicyErrors.None)
                            return true;

                        // log some stuff
                        return true;
                    }
                }).Name;

            services.AddTransient<NetscalerApiFactory>(sp => new NetscalerApiFactory(clientName, sp));

            return services;
        }
    }
}

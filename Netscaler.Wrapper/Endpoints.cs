﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netscaler.Wrapper
{
    public static class Endpoints
    {

        public static string NetScaler(string ip)
        {
            return $"https://{ip}/nitro/v1/";
        }
    }
}

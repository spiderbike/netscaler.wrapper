﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Netscaler.Wrapper.Models.TrafficManagement.LoadBalancing.VirtualServers;

namespace Netscaler.Wrapper
{
    public class NetScalerApi
    {
        private readonly HttpClient _httpClient;

        public NetScalerApi(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<TrafficManagementLoadBalancingVirtualServersListModel> ListVirtualServers()
        {
            var response = await _httpClient.GetAsync("config/lbvserver");

            await CheckResponse(response);

            return await response.Content.ReadAsAsync<TrafficManagementLoadBalancingVirtualServersListModel>();
        }

        public async Task<TrafficManagementLoadBalancingVirtualServersBindingsModel> ListVirtualServerBindings(string virtualServerName)
        {
            var response = await _httpClient.GetAsync($"config/lbvserver_service_binding/{virtualServerName}");

            await CheckResponse(response);

            return await response.Content.ReadAsAsync<TrafficManagementLoadBalancingVirtualServersBindingsModel>();
        }

        private static async Task CheckResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var stringResponse = await response.Content.ReadAsStringAsync();

                Console.WriteLine(stringResponse);

                response.EnsureSuccessStatusCode();
            }
        }
    }
}

﻿namespace Netscaler.Wrapper
{
    public class NetScalerOptions
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}